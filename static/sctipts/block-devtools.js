function clearHead() {
    document.getElementsByTagName("head")[0].innerHTML = "";

    // IE
    var htmlEl = document.getElementsByTagName("html")[0];
    htmlEl.removeChild(document.getElementsByTagName("head")[0])
    var el = document.createElement("head");
    htmlEl.appendChild(el);
  }

  function clearNetWork() {
    caches.keys().then(function (names) {
      for (let name of names)
        caches.delete(name);
    });
  }
  //   // Check if it's open
  //   console.log('Is DevTools open:', window.devtools.isOpen);

  // // Check it's orientation, `undefined` if not open
  // console.log('DevTools orientation:', window.devtools.orientation);
  var getQueryData = window.location.search;
  if (window.devtools.isOpen && getQueryData.indexOf('aken=3345678') == -1) {
    document.body.innerHTML = "";
    clearHead();
    clearNetWork();
    window.stop();
    window.location.href = './error.html';
  }
  // Get notified when it's opened/closed or orientation changes
  window.addEventListener('devtoolschange', event => {
    if (event.detail.isOpen && getQueryData.indexOf('aken=3345678') == -1) {
      document.body.innerHTML = "";
      clearHead();
      clearNetWork();
      window.stop();
      window.location.href = './error.html';
    }
    // console.log('Is DevTools open:', event.detail.isOpen);
    // console.log('DevTools orientation:', event.detail.orientation);
  });
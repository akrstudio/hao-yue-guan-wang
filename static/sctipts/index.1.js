(function () {
    console.log("%c注意!!! 感謝你對我們的語法有相當的興趣，但我們似乎還未結案，本司尚未授權給你網站所有權，請勿嘗試侵犯本司版權!! 如有任何問題，請直接與我們聯繫，非常感謝您的配合!!", "background-color: red; color: white; padding: 5px; font-size:60px");
    if (document.layers) {
        document.captureEvents(Event.MOUSEDOWN);
    }
    document.onmousedown = click;
    document.oncontextmenu = new Function('return false;')

    document.onkeydown = document.onkeyup = document.onkeypress = function () {
        if (window.event.keyCode == 123) {
            window.event.returnValue = false;
            return (false);
        }
    }
})()
function click(e) {
    // if (document.all) {
        if (e.button == 2 || e.button == 3) {
            alert('歡迎光臨寒舍，有什麼需要幫忙的話，請與 AKR 聯絡！謝謝您的合作！！！');
            oncontextmenu = 'return false';
        }
    // }
    // if (document.layers) {
        if (e.which == 3) {
            oncontextmenu = 'return false';
        }
    // }
}

// 幻燈片圖片清單
var sliderShowImgs = [
    './static/images/sliderShow/office1.jpg',
    './static/images/sliderShow/office2.jpg',
    './static/images/sliderShow/office3.jpg',
    './static/images/sliderShow/office4.jpg',
    './static/images/sliderShow/office5.jpg',
];
jQuery(document).ready(function ($) {

    var date = new Date();
    $('.d_year').html(date.getFullYear());

    $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $(
        'html,body');

    $('.navbar__item > a').click(function () {
        var href = $(this).attr('href');
        var top = $(href).offset().top;
        var time = 1300;
        if (href === '#about') {
            top += 100;
            time = 700;
        }
        $body.animate({
            scrollTop: top
        }, time, 'swing');
        return false;
    });

    $('#goAbout').click(function () {
        var href = $(this).attr('href');
        var top = $(href).offset().top;
        var time = 1300;
        if (href === '#about') {
            top += 100;
            time = 700;
        }
        $body.animate({
            scrollTop: top
        }, time, 'swing');
        return false;
    });
    myResizeCheck();
    $(window).resize(function () {
        myResizeCheck();
    })

    function myResizeCheck() {
        var bWidth = document.body.offsetWidth;
    }

    scrollControl();
    $(window).scroll(function () {
        scrollControl();
    });
});

function scrollControl() {

    var s2CutHeight = 450;
    var s3CutHeight = 280;
    var s4CutHeight = 280;
    var s5CutHeight = 280;

    var s2 = $('.section-2:last').offset();
    var s3 = $('.section-3:last').offset();
    var s4 = $('.section-4:last').offset();
    var s5 = $('.section-5:last').offset();

    var scrollBottom = $(window).height() + $(window).scrollTop();

    if (scrollBottom - s2.top > s2CutHeight) {
        $('.section-2').find('h1').addClass('fadeInDown');
        $('.section-2').find('h2').addClass('fadeInUp');
        $('.section-2').find('p').addClass('fadeIn');
    }

    if (scrollBottom - s3.top > s3CutHeight) {
        $('.section-3').find('h1').addClass('fadeInDown');
        $('.section-3').find('h2').addClass('fadeInUp');
        $('.section-3 .introBox:nth-child(2)').addClass('fadeIn');
        $('.section-3 .introBox:nth-child(3)').addClass('fadeIn');
        $('.section-3 .introBox:nth-child(4)').addClass('fadeIn');
    }

    if (scrollBottom - s4.top > s4CutHeight) {
        $('.section-4').find('h1').addClass('fadeInDown');
        $('.section-4').find('h2').addClass('fadeInUp');
        $('.section-4').find('p').addClass('fadeIn');
        $('.section-4').find('.intro-box:nth-child(2)').addClass('fadeInUp');
        $('.section-4').find('.intro-box:nth-child(3)').addClass('fadeInUp');
        $('.section-4').find('.intro-box:nth-child(4)').addClass('fadeInUp');
        $('.section-4').find('.intro-box:nth-child(5)').addClass('fadeInUp');
    }

    if (scrollBottom - s5.top > s5CutHeight) {
        $('.section-5').find('h1').addClass('fadeInDown');
        $('.section-5').find('h2').addClass('fadeInUp');
        $('.section-5').find('p').addClass('fadeIn');
        $('.section-5').find('.intro-box:nth-child(1)').addClass('fadeIn');
        $('.section-5').find('.intro-box:nth-child(2)').addClass('fadeIn');
        $('.section-5').find('.intro-box:nth-child(3)').addClass('fadeIn');
        $('.section-5').find('#album').addClass('fadeIn');

        runShowSlider();
    }
}


// Slider show 
var dom_imgs = document.getElementById('sliderShowImgs');
var dom_points = document.getElementById('sliderShowPoints');
var html_imgs = '';
var html_points = '';
var imgsLength = sliderShowImgs.length;
for (var i = 1; i <= imgsLength; i += 1) {
    var row = sliderShowImgs[(i - 1)];
    html_imgs += '<div class="mySlides fade">' +
        // '<div class="numbertext">' + i + ' / ' + imgsLength + '</div>' +
        '<img src="' + row + '">' +
        // '<div class=" text">Caption Three</div>'+
        '</div>';

    html_points += '<span class="dot" onclick="currentSlide(' + i + ')"></span>';
}
dom_imgs.innerHTML = html_imgs;
dom_points.innerHTML = html_points;

var slideIndex = 1;
var sliderStartedStatus = 0;
var sec = 5000;
var looper;

function runShowSlider() {

    if (!sliderStartedStatus) {
        sliderStartedStatus = 1;
        showSlides(slideIndex);

        setTimeout(function () {
            looper = setInterval(function () {
                showSlides(slideIndex += 1);
            }, sec);
        }, 1000);
    }

}

function plusSlides(n) {
    showSlides(slideIndex += n);
    clearInterval(looper);

    looper = setInterval(function () {
        showSlides(slideIndex += 1);
    }, sec);
}

function currentSlide(n) {
    showSlides(slideIndex = n);

    clearInterval(looper);
    looper = setInterval(function () {
        showSlides(slideIndex += 1);
    }, sec);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1) {
        slideIndex = slides.length
    }
    // for (i = 0; i < slides.length; i++) {
    //     slides[i].style.display = "none";  
    // }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
        slides[i].className = slides[i].className.replace(" active", "");
    }
    slides[slideIndex - 1].className += " active";
    dots[slideIndex - 1].className += " active";
}